import numpy as np
import pandas as pd
import requests as req
import io
import os
import logging 
import re
import sys


formatter = '%(name)s - %(levelname)s - %(asctime)s - %(message)s'



logging.basicConfig(stream=sys.stdout, level=logging.DEBUG , format=formatter)
class S3CsvLoader:
    def __init__(self,
                 PRODUCTS_CSV:str,
                 PRICES_STOCK_CSV:str):
        self.html_tag_reg=re.compile(r'<.*?>') 
        
        self.number_reg=re.compile(r'\b\d+\b')

        self.PRODUCTS_CSV = PRODUCTS_CSV
        self.PRICES_STOCK_CSV = PRICES_STOCK_CSV

        self.buffer_products = io.BytesIO()
        self.buffer_prices_stock= io.BytesIO()

        self.logger:logging.Logger = logging.getLogger('CSV_LOGGER')




        self.df_products=None
        self.df_buffer_prices_stock=None

    def download_csv_products(self):

        resp_prod = req.get(self.PRODUCTS_CSV)
        assert resp_prod.status_code == 200  , "Error fallo la carga del csv de productos"

        self.buffer_products.write(resp_prod.content) #*"el csv en el buffer para cargas mas rapidas
        self.buffer_products.seek(0)

    def download_csv_stock(self):
        resp_prices = req.get(self.PRICES_STOCK_CSV)
        assert resp_prices.status_code == 200  , "Error fallo la carga del csv de stocks"
        self.buffer_prices_stock.write(resp_prices.content) #*"el csv en el buffer para cargas mas rapidas
        self.buffer_prices_stock.seek(0)


    
    def run(self):
        self.logger.info("INICIANDO CARGA CSV PRODUCTOS")

        self.download_csv_products()

        self.logger.info("OK CARGA PRODUCTOS")
        yield "OK CARGA PRODUCTOS"

        self.logger.info("INICIANDO CARGA CSV STOCK")
        self.download_csv_stock()
        self.logger.info("OK CARGA STOCK")
        yield "OK CARGA STOCK"

        self.load_pandas()
        yield "ok cargado pandas"
        yield  self.process_pandas()
        self.logger.info("terminado de procesar")

    def run_dummy(self):
        
        self.logger.info("empezando dummy")

        with open("/home/guty/proyectos/PRODUCTS.csv" , "rb") as products:

            self.buffer_products.write(products.read())

            self.buffer_products.seek(0)

            yield "OK CARGA PRODUCTOS"
        
        with open("/home/guty/proyectos/PRICES-STOCK.csv" , "rb") as prices_stock:

            self.buffer_prices_stock.write(prices_stock.read())
            self.buffer_prices_stock.seek(0)
            yield "OK CARGA STOCK"

            
        self.logger.info("Se termino la carga de archivos")
        self.load_pandas()
        self.logger.info("Se cargo en memoria")
        yield "ok cargado pandas"
        self.logger.info("Empezando a procesar")
        
        yield  self.process_pandas()
        self.logger.info("terminado de procesar")

        



        
        
    def load_pandas(self):
        self.df_products = pd.read_csv(self.buffer_products, sep="|")
        self.df_prices_stock = pd.read_csv(self.buffer_prices_stock, sep="|")


    def process_pandas(self):

        
        #!PRODUCTOS
        products = self.df_products
        prices_stock = self.df_prices_stock

        # sku_unicos = prices_stock["SKU"].unique()

        products_unicos:pd.DataFrame = products.drop_duplicates(subset=["SKU"])
        
        self.clean_items(df=products_unicos)

        products_unicos["CAT_AND_SUB"] = products_unicos.apply(lambda row: "%s|%s|%s"%(row["CATEGORY"].lower(), row["SUB_CATEGORY"] , row["SUB_SUB_CATEGORY"]), axis=1)

        products_unicos["PACKAGE"] = products_unicos.apply( self.buscar_package,axis=1)
        

        self.logger.info("SE TERMINO DE PROCESAR PRODUCTOS")
        
        # nombre_columnas = list(products_unicos.columns)
        

        #*PRICES
        valid_prices:pd.DataFrame = prices_stock[prices_stock["STOCK"] > 0]

        self.logger.info("SE TERMINO DE PROCESAR PRECIOS")
        

        self.logger.info("Limpiando buffer de memoria")
        self.buffer_prices_stock.truncate(0)
        self.buffer_products.truncate(0)
        self.logger.info("Terminado - Limpiando buffer de memoria")


        

        return products_unicos , valid_prices 
        
    def clean_items(self , df) :
        for col in df.columns:
            df[col].fillna("", inplace=True)
            self.remove_html_tags(df=df, col=col)


            

    def remove_html_tags(self, df , col) :

        if str(df[col].dtypes) not in ("int64" , "int32" , "bool") :

            df[col] = df[col].apply(lambda current: 
            self.html_tag_reg.sub("",current) if type( current) == str else current)


    def buscar_package(self , row) :
        pos_numbers = list(self.number_reg.finditer(row["ITEM_DESCRIPTION"]))




        if len(pos_numbers)> 0 :
            return row["ITEM_DESCRIPTION"][pos_numbers[-1].start():].replace("." ,"")
        else :
            return ""
        
class UserToken:
    access_token:str
    expires_in:int
    refresh_token:str
    token_type:str

    def __init__(self,
                 access_token: str,
                 expires_in: int,
                 refresh_token: str,
                 token_type: str,
                 ):
        self.access_token = access_token
        self.expires_in = expires_in
        self.refresh_token = refresh_token
        self.token_type = token_type


class Merchant:

    can_be_deleted:bool = None
    can_be_updated:bool = None
    id:str = None
    is_active:bool = None
    name:str = None

    def __init__(self,

                 can_be_deleted: bool = None,
                 can_be_updated: bool = None,
                 id: str = None,
                 is_active: bool = None,
                 name: str = None,
                 ):

        self.can_be_deleted = can_be_deleted
        self.can_be_updated = can_be_updated
        self.id = id
        self.is_active = is_active
        self.name = name


class CornerShopAPI():
    def __init__(self) : 
        self.GRAND_TYPE= os.getenv("GRAND_TYPE")
        self.CLIENT_ID= os.getenv("CLIENT_ID")
        self.CLIENT_SECRET= os.getenv("CLIENT_SECRET")
        self.API=os.getenv("API") 
        self.logger:logging.Logger = logging.getLogger('API_LOGGER')

        self.user_token: UserToken = None
        self.token_header :str= None
        self.merchants = []
        self.current_merchant: Merchant = None

    def get_credentials(self):
        url = f"{self.API}/oauth/token"



        params = {
            "client_id": self.CLIENT_ID,
            "client_secret": self.CLIENT_SECRET,
            "grant_type": self.GRAND_TYPE,
        }

        try:
            credentials = req.request(method="POST", url=url, params=params)

            if credentials.status_code==200 : 
                json_credentials = credentials.json()
                self.logger.info(json_credentials)
                self.user_token = UserToken(**json_credentials)
                self.token_header = self.user_token.access_token
            

            
        except Exception as ex:

            self.logger.error("Error en la obtencion de las credenciales")

            self.logger.error(str(ex.args))

            raise Exception( "Error obtener credenciales")
    

            
    def get_merchants(self , name:str="Richard")->bool:

        url = f"{self.API}/api/merchants"

        headers = {'token': f'Bearer {self.token_header}'}

        merchants = req.get(url, headers=headers)

        if merchants.status_code==200:

            json_merchants= merchants.json()

            filtered_merchants = filter(lambda row: name in row["name"], json_merchants["merchants"])

            if filtered_merchants :
                self.current_merchant = next(filtered_merchants)

                self.current_merchant = Merchant(**self.current_merchant)

                self.merchants = json_merchants["merchants"]

                return True
            else:
                raise Exception("Error no existe el usuario solicitado")
            

    def update_current_merchant(self ,data_merchant:Merchant =None)->bool:

        url = f"{self.API}/api/merchants/{self.current_merchant.id}"
        fields = ["can_be_deleted",
                  "can_be_updated",
                  "is_active",
                  "name"]

        json_field = {}

        for field in fields:

            cur_val = getattr(data_merchant , field)

            
            if cur_val:
                json_field[field] = cur_val
            


        headers = {'token': f'Bearer {self.token_header}'}


        try:

            updated_current_merchant = req.put(url=url,
                                            headers=headers,
                                            json=json_field)

            if updated_current_merchant.status_code==200:
                for field in fields:
                    cur_val = getattr(data_merchant , field)

                    if cur_val:
                        setattr(self.current_merchant, field, cur_val)
                return True
          
                
               

          
        except Exception as ex:

            import traceback as tb
            tb.print_exc()
            raise ex

    def delete_merchant(self , name = "Beauty")->bool:

        url = f"{self.API}/api/merchants/{merchant_id}"        
        merchant_id = None

        for pos, merchant in enumerate(self.merchants):

            if merchant["name"] == name  :
                merchant_id =merchant["id"]
                merchant_pos = pos
        if merchant_id is None :
            raise Exception(f"No se pudo encontrar el mercader con nombre {name}")
        
        headers = {'token': f'Bearer {self.token_header}'}




        borrado = req.delete(url=url, headers=headers)

        if borrado.status_code==200 : 

            del self.merchants[merchant_pos]

            return True
        else :
            raise Exception("No se pudo borrar el mercader")

    def ingresar_producto(self ,products:pd.DataFrame , prices:pd.DataFrame):


        url = f"{self.API}/api/products"
        headers = {'token': f'Bearer {self.token_header}'}
        sorted_prices: pd.DataFrame = prices.sort_values(by="PRICE",
                                                         axis=0,
                                                         ascending=False)

        sorted_prices = sorted_prices.dropna(subset=["BRANCH"] ,axis=0)

        sorted_prices = sorted_prices[:100]
        
        general = pd.merge(products, sorted_prices, on="SKU")

        unique_sku=sorted_prices["SKU"]
        
        
        for sku in unique_sku:
            current_rows = general[general["SKU"] == sku]

            first_row = current_rows.iloc[0]
            # print (first_row)
            
            
            json_data = {
                "merchant_id": self.current_merchant.id,
                "sku": str(sku),
                "barcodes": None,
                "brand": first_row.BRAND_NAME,
                "name": first_row.ITEM_NAME,
                "description": first_row.DESCRIPTION_STATUS,
                "package": first_row.PACKAGE,
                "image_url": first_row.ITEM_IMG,
                "category": first_row.CAT_AND_SUB,
                "url": None,
                "branch_products": []
            }

            json_data["branch_products"] = [
                {
                    "branch": row["BRANCH"],
                    "stock": row["STOCK"],
                    "price": row["PRICE"],
                }

                for index, row in current_rows.iterrows()]
            
            resp=req.post(url=url, json=json_data, headers=headers)

            if resp.status_code !=200 : 
                print(resp.content)
                raise Exception("Error en el envio de datos")

                


            
    def run(self,
            products: pd.DataFrame,
            prices: pd.DataFrame,
            merchant_name: str = "Richard",
            merchant_to_delete: str = "Beauty",):

        self.get_credentials()
        self.logger.info("credenciales obtenidas")
        yield "CREDENCIALES OBTENIDAS"

        self.get_merchants(name=merchant_name)
        self.logger.info("obtenido mercader")
        yield "OBTENIDO MERCADER"

        self.update_current_merchant(data_merchant=Merchant(is_active=True))
        self.logger.info("actualizado merchant")
        yield "ACTUALIZADO MERCHANT"

        # self.delete_merchant(name=merchant_to_delete)

        self.logger.info("Borrado mercader")

        yield "Borrado mercader"
        self.logger.info("terminado procesamiento e ingreso")
        yield self.ingresar_producto(products= products , prices=prices)

         














        
        
        
