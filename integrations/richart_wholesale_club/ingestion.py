
from utils import S3CsvLoader,CornerShopAPI
import os
import logging
import time




def process_csv_files():
    
    GRAND_TYPE = os.getenv("GRAND_TYPE", None)
    CLIENT_ID = os.getenv("CLIENT_ID", None)
    CLIENT_SECRET = os.getenv("CLIENT_SECRET", None)
    PRODUCTS_CSV = os.getenv("PRODUCTS_CSV_URL", None)
    PRICES_STOCK_CSV = os.getenv("PRICES_STOCK_CSV", None)
    ini = time.time()
    csv_loader=S3CsvLoader(
        PRODUCTS_CSV=PRODUCTS_CSV,
        PRICES_STOCK_CSV=PRICES_STOCK_CSV
    )
    csv_loader.logger.info("INICIADO PROGRAMA - CARGANDO CSV")

    run_generator = csv_loader.run()

    carga_productos = next(run_generator)
    carga_stocks = next(run_generator)
    carga_pandas = next(run_generator)

    products, prices = next(run_generator)

    csv_loader.logger.info("Terminado ejecucion")

    api = CornerShopAPI()

    api.logger.info("Iniciando API")

    run_api_generator = api.run(
        merchant_name="Richard",
        merchant_to_delete="Beauty",
        products=products,
        prices=prices)

    

    credenciales = next(run_api_generator)

    mercader = next(run_api_generator)
    
    update_merchant = next(run_api_generator)

    delete_merchant =next(run_api_generator)

    send_product = next(run_api_generator)

    fin = time.time()

    tiempo_ejecucion = fin-ini

    api.logger.info("Duracion de ejecucion %s" % (str(tiempo_ejecucion)))

    api.logger.info("TERMINADO")

if __name__ == "__main__":

    
    process_csv_files()
